package com.mitocode.model;

public enum TipoCuenta {

	SOLES, DOLARES, EUROS
}
