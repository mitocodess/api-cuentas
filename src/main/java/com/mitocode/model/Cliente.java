package com.mitocode.model;

import java.util.ArrayList;

public class Cliente {

	private Long id;
	private String apellidos;
	private String nombres;
	private String email;
	private ArrayList<Cuenta> cuentas;

	public Cliente() {
		super();
	}

	public Cliente(Long id, String apellidos, String nombres, String email, ArrayList<Cuenta> cuentas) {
		super();
		this.id = id;
		this.apellidos = apellidos;
		this.nombres = nombres;
		this.email = email;
		this.cuentas = cuentas;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getApellidos() {
		return apellidos;
	}

	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}

	public String getNombres() {
		return nombres;
	}

	public void setNombres(String nombres) {
		this.nombres = nombres;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public ArrayList<Cuenta> getCuentas() {
		return cuentas;
	}

	public void setCuentas(ArrayList<Cuenta> cuentas) {
		this.cuentas = cuentas;
	}

}
