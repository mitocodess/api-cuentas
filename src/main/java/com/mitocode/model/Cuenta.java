package com.mitocode.model;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.UUID;

public class Cuenta {

	private final UUID id;
	private String numeroCuenta;
	private BigDecimal saldo;
	private TipoCuenta tipoCuenta;
	private ArrayList<Transaccion> transacciones;

	public Cuenta(UUID id, String numeroCuenta, BigDecimal saldo, TipoCuenta tipoCuenta,
			ArrayList<Transaccion> transacciones) {
		super();
		this.id = id;
		this.numeroCuenta = numeroCuenta;
		this.saldo = saldo;
		this.tipoCuenta = tipoCuenta;
		this.transacciones = transacciones;
	}

	public String getNumeroCuenta() {
		return numeroCuenta;
	}

	public void setNumeroCuenta(String numeroCuenta) {
		this.numeroCuenta = numeroCuenta;
	}

	public BigDecimal getSaldo() {
		return saldo;
	}

	public void setSaldo(BigDecimal saldo) {
		this.saldo = saldo;
	}

	public TipoCuenta getTipoCuenta() {
		return tipoCuenta;
	}

	public void setTipoCuenta(TipoCuenta tipoCuenta) {
		this.tipoCuenta = tipoCuenta;
	}

	public ArrayList<Transaccion> getTransacciones() {
		return transacciones;
	}

	public void setTransacciones(ArrayList<Transaccion> transacciones) {
		this.transacciones = transacciones;
	}

	public UUID getId() {
		return id;
	}

}
