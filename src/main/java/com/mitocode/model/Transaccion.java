package com.mitocode.model;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.UUID;

public class Transaccion {

	private final UUID id;
	private String concepto;
	private LocalDate fecha;
	private BigDecimal importe;

	public Transaccion(UUID id, String concepto, LocalDate fecha, BigDecimal importe) {
		super();
		this.id = id;
		this.concepto = concepto;
		this.fecha = fecha;
		this.importe = importe;
	}

	public String getConcepto() {
		return concepto;
	}

	public void setConcepto(String concepto) {
		this.concepto = concepto;
	}

	public LocalDate getFecha() {
		return fecha;
	}

	public void setFecha(LocalDate fecha) {
		this.fecha = fecha;
	}

	public BigDecimal getImporte() {
		return importe;
	}

	public void setImporte(BigDecimal importe) {
		this.importe = importe;
	}

	public UUID getId() {
		return id;
	}

}
