package steps;

import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.Matchers.empty;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.restassured.RestAssured;
import io.restassured.response.Response;

public class StepsListarClientes {

	private Response response = null;

	@Given("hago un get a {string}")
	public void hago_un_get_a(String context) {
		String path = "http://localhost:9090".concat(context);
		response = RestAssured.when().get(path);
	}

	@Then("compruebo que la respuesta http tiene el codigo de estado {int}")
	public void compruebo_que_la_respuesta_http_tiene_el_codigo_de_estado(Integer statusCode) {
		response.then().statusCode(statusCode);
	}

	@Then("compruebo que el array {string} no esta vacio")
	public void compruebo_que_el_array_no_esta_vacio(String jsonPath) {
		response.then().body(jsonPath, not(empty()));
	}

}
