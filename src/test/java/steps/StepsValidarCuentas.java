package steps;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.Matchers.emptyOrNullString;
import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.Matchers.lessThanOrEqualTo;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import net.serenitybdd.rest.SerenityRest;

public class StepsValidarCuentas {
	
	private String endpoint = "http://localhost:9090";

	@Given("hago un get a {string} con parametro id de valor {int}")
	public void hago_un_get_a_con_parametro_id_de_valor(String path, Integer id) {
	    SerenityRest.when().get(endpoint.concat(path).concat(id.toString())).andReturn();
	}

	@Then("compruebo que la propiedad {string} no esta vacia ni nulo")
	public void compruebo_que_la_propiedad_no_esta_vacia_ni_nulo(String propiedad) {
		SerenityRest.lastResponse().then().assertThat().body(propiedad, not(emptyOrNullString()));
	}

	@Then("compruebo que la propiedad {string} tiene de valor {string}")
	public void compruebo_que_la_propiedad_tiene_de_valor(String propiedad, String valor) {
		SerenityRest.lastResponse().then().assertThat().body(propiedad, is(valor));
	}

	@Then("compruebo que el array {string} es mayor que {int}")
	public void compruebo_que_el_array_es_mayor_que(String propiedad, Integer valor) {
		SerenityRest.lastResponse().then().assertThat().body(propiedad.concat(".size()"), greaterThan(valor));
	}

	
}
