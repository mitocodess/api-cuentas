FROM openjdk:11
WORKDIR /workspace
COPY target/api-cuentas-*.jar app.jar
EXPOSE 9090
ENTRYPOINT [ "java", "-jar", "/workspace/app.jar" ]
